# PRÁCTICAS MÓDULO CLOUD

## Preparación

* Cuenta de [Docker Hub](https://hub.docker.com/)
* Cuenta de [AWS](http://aws.amazon.com/)

## Práctica 1

Dado este proyecto en NodeJS, crea su Dockerfile sabiendo que nos han pedido como imagen base ubuntu:18.04, versión 10 de NodeJS, el 8888 será el puerto donde exponga la comunicación la applicación, la señal de *STOP* debe llegarle a la aplicación y el contenedor podría ser iniciado con cualquier proceso.
* [Gitlab](https://gitlab.com/master5-lemoncode/cloud/ejercicio-cloud-lab)

## Práctica 2

Sube la imagen de Docker a DockerHub.
* [Docker Hub](https://hub.docker.com/r/dockerhubandonitf/ejercicio-cloud-lab)

## Práctica 3

Automatiza el proceso de creación de la imagen de Docker y su subida a Docker Hub después de cada cambio en el repositorio.

## Práctica 4

Crea un servidor y despliega la imagen de Docker en AWS utilizando Terraform.
* [Aplicación en AWS](http://18.202.36.105)