# Seleccionar imagen base
FROM ubuntu:18.04

# Instalar nodejs v10
RUN apt-get update && apt-get install -y curl build-essential && curl -sL https://deb.nodesource.com/setup_10.x | bash - && apt-get install -y nodejs

# Directorio de trabajo
WORKDIR /opt/ejercicio-cloud-lab

# Copiar ficheros en la imagen
COPY . .

# Instalar dependencias
RUN npm install --only=prod

# Exponer puerto
EXPOSE 8888

# Command
CMD ["npm", "start"]
